/*"use strict";*/
// Exercice: Manipulation de tableaux avec boucles et fonctions en javascript

// Partie 1 : Creation et manipulation tableau
// 1. Création d'un tableau vide appelé "nombres"
let nombres = [];
 
// 2. Ajout de nombre de 1 à 10 dans le tableau "nombres" à l'aide d'une boucle
  for (let i = 1; i <= 10; i++){
    nombres.push(i);
  }
	console.log(nombres);// Affichage du contenu "nombres" dans la console


// Partie 2 : Utilisation des fonction sur les tableaux

// 4. Creer une fonction nommee "sommeTableau" qui prend en parametre un tableau de nombre et retourne la somme de tous les nombres dans le tableau
let tab = []
function sommeTableau(tab) {
  let sum = 0;
 for (let i = 0; i < tab.length; i++ )
  sum += tab[i];
  return sum;
}
console.log(sommeTableau(nombres)); //5. Affichage du resultat dans la console


// Partie 3 : Manipulation avancée de tableau avec boucles et conditions
// 6. Crée une fonction nommée "filtrerPaires" qui prend en parametre un tableau de nbre et retourne un nveau tab contenant seulement des nbres paires tab dorigine
let nbre = []
function filtrerPaires(nbre){
 let result = nbre.filter(n => n%2 === 0);
 console.log(result);
}
filtrerPaires(nombres); // 7. Appeler la fonction en passant le tableau "nombres" en argument et affiche le resultat dans la console